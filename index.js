const express = require('express')
const app = express()
const port = 56201
const bodyParser = require('body-parser');
app.use(bodyParser.text());


app.post('/square',(req,res) => {
    const num = +req.body;
    const result = num * num;
    res.send({
        number: num,
        square: result
    });
});



function reverseString(str) {
    var newString = "";
    for (var i = str.length - 1; i >= 0; i--) {
        newString += str[i];
    }
    return newString;
}

app.post('/reverse',(req,res) => {
    res.send(reverseString(req.body));
});



function isYearLeap(year) {return ((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0);}

const daysNames = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

app.get('/date/:year/:month/:day', (req,res) => {
    const {year, month, day} = req.params;
    const date = new Date(year, month - 1, day);
    const currentDay = new Date();
    res.send({
        weekDay: daysNames[day],
        isLeapYear: isYearLeap(year),
        difference: Math.abs(Math.floor((currentDay.getTime() - date.getTime()) / (1000 * 3600 * 24)))
    })
})


app.listen(port, () => {
    console.log("Example app listening on port ${port}")
})